\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[a4paper,left=2cm,right=2cm,top=2.7cm,bottom=2cm]{geometry}
\usepackage{lmodern}
\usepackage{layout}
\usepackage{caption}
\usepackage{pifont}
\usepackage{shapepar}
\usepackage[absolute,showboxes,overlay]{textpos}
\usepackage[Lenny]{fncychap}
\usepackage{amssymb}
\usepackage{enumerate}
\usepackage[pdftex]{graphicx}
\usepackage{fancyhdr}
\usepackage{enumitem}
\usepackage{color}
\usepackage{colortbl}
\usepackage{caption}
\usepackage{enumerate}
\usepackage{setspace}

\renewcommand{\chaptername}{}
\renewcommand{\contentsname}{Table des Matières}
\renewcommand{\bibname}{Bibliographie}
\renewcommand{\thesection}{\arabic{section}}

\newcommand{\hsp}{\hspace{10pt}}
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

\setlength{\parindent}{1cm}
\setlength{\parskip}{1ex plus 0.5ex minus 0.2ex}

\begin{document}

\begin{titlepage}
  \begin{center}

    % Upper part of the page. The '~' is needed because \\
    % only works if a paragraph has started.
    \includegraphics[scale=0.5]{img/fsg.jpg}\\[1.5cm]
    
    \begin{minipage}{13cm}
    \centering
      \normalsize{ \textbf{Département d'informatique et de génie logicielle}}\\
    \end{minipage}

       \vspace{2cm}
    % Title
    \medskip
    \medskip
    IFT-7009 -- Réseaux mobiles --\\
     \vspace{2cm}
   
    \HRule \\[0.4cm]
    { \large \bf \underline{\emph{Theme du rapport :}} \\  \vspace{0.7cm} Services géolocalisés à valeur ajoutée \\[0.4cm] }

    \HRule \\[2cm]
    % Author and supervisor
    \begin{minipage}{0.3\textwidth}
      \begin{center} 
      
       \textbf{\emph{Réalisé Par :}}\\
       \vspace{0.3cm}
        \textsc{Kalmogo Roland} \emph{,} \textsc{Kristou Haithem}\\
        
      \end{center}
    \end{minipage}
    \hspace{6cm}
    \begin{minipage}{0.3\textwidth}
       \begin{center}  \small
        \textbf{\emph{Enseignant :}}\\
        \vspace{0.7cm}
        \textsc{Ronald Beaubrun}\\
      \end{center}
    \end{minipage}

    \vfill

    % Bottom of the page
     Automne 2015 

  \end{center}

\end{titlepage}

\pagestyle{fancy}
%\renewcommand{\headheight}{10pt}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0.7pt}
\renewcommand{\headsep}{23pt}
\lhead{}
\rhead{\leftmark}
\lfoot{Services géolocalisés à valeur ajoutée}
\rfoot{K. Roland, K. Haithem}
\cfoot{\thepage}

\tableofcontents

% place to include docs
\include{introduction}
\include{services}
\include{conclusion}
\bibliographystyle{plain}
\addcontentsline{toc}{chapter}{Bibliographie}
\bibliography{refs}
\end{document}