\documentclass[xcolor=dvipsnames,hyperef=colorlinks]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{layout}
\usepackage{graphicx}
\usepackage{pifont}
\usepackage{shapepar}
\usepackage[absolute,showboxes,overlay]{textpos}
% \usepackage[Lenny]{fancychap}
\usepackage{color}
\usepackage{colortbl}
%\usepackage{caption}
\usepackage{enumerate}

\usetheme{Warsaw}
\usecolortheme[named=BrickRed]{structure}
\author{Roland KALMOGO, Haithem KRISTOU}

\title{Services géolocalisés à valeur ajoutée}
\hypersetup{pdfpagemode=FullScreen}
% \addtobeamertemplate{headline}{\hfill\insertframenumber/\inserttotalframenumber\hspace{2em}\null}


%*************Couleur personalisee des blocks************************

\beamerboxesdeclarecolorscheme{bloc1}{Maroon}{Peach}
\definecolor{vertmoyen}{RGB}{51,110,23}



\AtBeginSection[]{
  
   \begin{frame}[plain]
    \transboxout<1>[duration=1]
     \tableofcontents[currentsection,hideothersubsections]
   \end{frame} 
  
}

% * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
% * * * * * * * * * * * * * * Page de Garde * * * * * * * * * * * * * * * * * *

\begin{document}
\begin{frame}[t, plain]
\begin{center}
  \includegraphics[scale=0.34]{img/fsg.jpg}\\
  \begin{exampleblock}
   
      \centering \textbf{ IFT-7009}\\
	\centering --  Réseaux mobiles  --
    
  \end{exampleblock}
\end{center}
 \begin{center}
  \textcolor{vertmoyen}{ \textbf{ \Large Présentation du rapport  }}
  \begin{exampleblock} {}
    \centering \underline{Theme:} \hspace{0.4cm}  Services géolocalisés à \emph{valeur ajoutée}
  \end{exampleblock}
 \end{center}
 \begin{columns}
	 \begin{column}{4.5cm}
	  \begin{alertblock}{Réalisé par :}
		      \small \textsc{Kalmogo} Roland \\
		      \small \textsc{Kristou} Haithem \\
		  \end{alertblock}
	      \end{column}
	      \hspace{2cm}
	      \begin{column}{4.5cm}
		  \begin{alertblock}{Enseignant:}
		     \small Ronald \textsc{Beaubrun} \\
		      \footnotesize Enseignant à l'Université Laval
		  \end{alertblock}
		\end{column}
	\end{columns}
\end{frame}
    
\begin{frame}[t, plain]{Plan \hspace{10.5cm} 1}
    \tableofcontents[hideallsubsections,pausesections]
\end{frame}
    
   %****************FIN PAGE DE GARDE*************************
    
    \section{Introduction }
\subsection{Définition de la géolocalisation}
    \begin{center}    
    \begin{frame} {Qu'est ce que c'est que la géolocalisation}
    \begin{columns}
	      \begin{column}{5cm}
		  \begin{figure}[htp]
		    \centering
		      \includegraphics[scale=0.23]{img/geo.jpg}\\
		      
		    \end{figure}
	      \end{column}
	      \hspace{1cm}
	      \begin{column}{5cm}
		  \pause
	
		    \begin{list}{}{}	 
			\begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering Positionner un objet sur un plan  
			\end{exampleblock}
			\pause
			\begin{exampleblock}{}\hspace{0.05cm } \item  $\clubsuit$ \centering Objet Solide  \end{exampleblock}
			\pause
			 \begin{exampleblock}{}\hspace{0.05cm } \item  $\clubsuit$ \centering Objet immatériel  \end{exampleblock}
	
	 
		    \end{list}
% 			    
		\end{column}
	\end{columns}

	    \end{frame}
    \end{center}
\subsection{Historique de la géolocalisation}
    \begin{center}    
    \begin{frame} {Historique de la géolocalisation}
	\pause
	 \begin{columns}
	      \begin{column}{5cm}
		  \begin{figure}[htp]
		    \centering
		      \includegraphics[scale=0.23]{img/homme-prehistorique.jpg}\\
		      
		    \end{figure}
	      \end{column}
	      \hspace{1cm}
	      \begin{column}{5cm}
		  \pause
	
		    \begin{list}{}{}	 
			\begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering Un besoin pour survivre \end{exampleblock}

			\pause
			\begin{exampleblock}{}\hspace{0.05cm } \item  $\clubsuit$ \centering Un langage commun   \end{exampleblock}

		    \end{list}
% 			    
		\end{column}
	\end{columns}
	
	    \end{frame}
    \begin{frame} {L'Égypte Antique}
    \begin{columns}
	      \begin{column}{5cm}
		  \begin{figure}[htp]
		    \centering
		      \includegraphics[scale=0.23]{img/obelisque-egyptienne.jpg}\\
		      
		    \end{figure}
	      \end{column}
	      \hspace{1cm}
	      \begin{column}{5cm}
		  
	
		    \begin{list}{}{}	
			  \begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering Premier système de signalisation: Les obélisques \end{exampleblock}
			  \pause
			 \begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering Géomarketing \end{exampleblock}
			  
		    \end{list}
% 			    
		\end{column}
	\end{columns}
	\end{frame}
\begin{frame} {La Grèce Antique}
\pause
	
    	\begin{list}{}{}	 
	 \begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering géolocalisation par les astres \end{exampleblock}
    	\end{list}

    		\begin{figure}[htp]
	      \centering
	      \includegraphics[scale=0.63]{img/observateur.jpg}\\
	     
	  \end{figure}
 \end{frame}
 \begin{frame}{La Grèce Antique}
    	\begin{list}{}{}	 
	 \begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering Invention de l’astrolabe \end{exampleblock}
        \end{list}
    	
    	\begin{figure}[htp]
	      \centering
	      \includegraphics[scale=0.63]{img/astrolabe.jpg}\\
	     
	  \end{figure}

\end{frame}

\begin{frame} {Naissance de la géolocalisation moderne}
	\pause
	 \begin{columns}
	      \begin{column}{5cm}
	      \begin{list}{}{}	 
		    \begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering En temps réel \end{exampleblock}
		    \pause
		    \begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering la découverte de la radio-navigation  \end{exampleblock}
	    \end{list}
		 
	      \end{column}
	      \hspace{1cm}
	      \begin{column}{5cm}
		   \begin{figure}[htp]
		    \centering
		      \includegraphics[scale=0.33]{img/debarquement.jpg}\\
		      
		    \end{figure}
% 			    
		\end{column}
	\end{columns}
	\end{frame}
	\begin{frame} {Naissance de la géolocalisation moderne}
	
    	\begin{list}{}{}	 
	
	 \begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering la découverte des antennes directionnelles et des ondes électromagnétiques   \end{exampleblock}
	 

	 	 \begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering la découverte des techniques de localisation par différence de temps observé  \end{exampleblock}

	 \end{list}


\end{frame}
\end{center}

\section{Différentes approches dans la géolocalisation}
\subsection{Géolocalisation par satellite}
\begin{center}
 \begin{frame} {Géolocalisation par satellite }
\pause
	 \begin{columns}
	      \begin{column}{5cm}
	      \begin{list}{}{}	 
		  \begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering Système GPS    \end{exampleblock}

		     \begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering Composé de trois parties distinctes \end{exampleblock}
		    
		   \begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering Une précision pouvant aller de 15 à 100 mètres pour les applications civiles    \end{exampleblock}

	\end{list}
		 
	      \end{column}
	      \hspace{1cm}
	      \begin{column}{5cm}
		   \begin{figure}[htp]
		    \centering
		      \includegraphics[scale=0.43]{img/gps.png}\\
		      
		    \end{figure}
% 			    
		\end{column}
	\end{columns}
	
    	\begin{list}{}{}	 
	 \begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering Composé de trois parties distinctes \end{exampleblock}
	 \begin{figure}[htp]
	      \centering
	      \includegraphics[scale=0.53]{img/gps.png}\\
	      \caption{Système GPS}
	      \label{figure:}
	  \end{figure}
	 
	 \begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering Une précision pouvant aller de 15 à 100 mètres pour les applications civiles    \end{exampleblock}
	 
	 \end{list}


\end{frame}
\subsection{Géolocalisation par téléphone portable}
 \begin{frame} {Géolocalisation par téléphone portable }
\pause
    	\begin{list}{}{}	 
	 \begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering Positionnement par GSM \end{exampleblock}
	 
	 \begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering Numéro IMEI\end{exampleblock}
	 
	 \begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering La méthode Cell-id \end{exampleblock}
	 
	 \begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering Ne requiert  aucune infrastructures supplémentaires \end{exampleblock}

	  \begin{exampleblock}{}\hspace{0.05cm} \item    $\clubsuit$ \centering Mais, manque de précision \end{exampleblock}
	
	 
	 \end{list}


\end{frame}

 
 
\end{center}

    
    
    
    \section{Services de localisation à valeur ajoutée}
    \subsection{Définition}
    \begin{center}
      \begin{frame}{Qu'est ce qu'un service géolocalisé? \hfill\insertframenumber/\inserttotalframenumber\hspace{0.3em}\null}
	  
	  \pause
	   \begin{list}{}{}
	    
	    \begin{exampleblock}{} \hspace{0.05cm} \item  $\clubsuit$ \centering Service d'information \end{exampleblock}
	                               
	    \pause
	    
	    \begin{exampleblock}{}\hspace{0.05cm } \item  $\clubsuit$ \centering Position \end{exampleblock}
	   \end{list}
      \end{frame}
    \end{center}

  \subsection{Fonctionnement des services géolocalisés}
    \begin{center}
      \begin{frame}{Comment fonctionnent ces services ? \hfill\insertframenumber/\inserttotalframenumber\hspace{0.3em}\null}
	  
	  \pause
	   \begin{figure}[htp]
	      \centering
	      \includegraphics[scale=0.63]{img/LBSposi.jpg}\\
	      \caption{Contexte des services basés sur la géolocalisation}
	      \label{figure:}
	  \end{figure}
      \end{frame}
      \begin{frame}[t, plain]
% 		 \frametitle{\scriptsize Architecture d'un LBS \hfill\insertframenumber/\inserttotalframenumber\hspace{0.3em}\null}
			\begin{figure}[htp]
			    \centering
			    \includegraphics[scale=0.43]{img/lbs_arch.jpg}\\
			    \caption{Infrastructures des services basés sur la géolocalisation}
			    \label{figure:}
			\end{figure}
      \end{frame}
    \end{center}
    \subsection{Classification des services}
    \begin{center}
      \begin{frame}{Qui est-ce qui initie la requête ? \hfill\insertframenumber/\inserttotalframenumber\hspace{0.3em}\null}
	  
	  \pause
	   \begin{list}{}{}
	    
	    \begin{exampleblock}{} \hspace{0.05cm} \item  $\clubsuit$ \centering Pull request \end{exampleblock}
	                               
	    \pause
	    
	    \begin{exampleblock}{}\hspace{0.05cm } \item  $\clubsuit$ \centering Push request \end{exampleblock}
	   \end{list}
      \end{frame}
      \begin{frame}{L'utilisateur initie la requête}
% 		 \frametitle{\scriptsize Architecture d'un LBS \hfill\insertframenumber/\inserttotalframenumber\hspace{0.3em}\null}
			\begin{figure}[htp]
			    \centering
			    \includegraphics[scale=0.47]{img/pull.png}\\
			    \caption{Services de type pull}
			    \label{figure:}
			\end{figure}
      \end{frame}
      \begin{frame}{Le service initie la requête}
% 		 \frametitle{\scriptsize Architecture d'un LBS \hfill\insertframenumber/\inserttotalframenumber\hspace{0.3em}\null}
			\begin{figure}[htp]
			    \centering
			    \includegraphics[scale=0.47]{img/push.png}\\
			    \caption{Services de type push}
			    \label{figure:}
			\end{figure}
      \end{frame}
      \subsection{Quelques exemples d'applications}
      \begin{frame}{Des exemples ...}
		\begin{columns}
	      \begin{column}{5cm}
		  \begin{alertblock}{Applications de divertissement}
		      \begin{list}{$\clubsuit$}{}
			   \pause
			    \item Rencontres
 			   \pause
			    \item Jeu en ligne
% 			    
			\end{list}
		  \end{alertblock}
	      \end{column}
	      \hspace{1cm}
	      \begin{column}{5cm}
		  \begin{alertblock}{Applications de mobilité}
		     \begin{list}{$\clubsuit$}{}
 			   \pause
			    \item Routière et navigation
 			   \pause
			    \item Guide pour villes
% 			    
			\end{list}
		  \end{alertblock}
		\end{column}
	\end{columns}
		
	\begin{columns}
	\begin{column}{4.5cm}
		  \begin{alertblock}{Applications d’urgence}
		     \begin{list}{$\clubsuit$}{}
 			   \pause
			    \item Traçage 
 			   \pause
			    \item Garde du corps
% 			    
			\end{list}
		  \end{alertblock}
	      \end{column}
	      \hspace{1cm}
	      \begin{column}{4.5cm}
		  \begin{alertblock}{Applications de e-commerce}
		     \begin{list}{$\clubsuit$}{}
 			   \pause
			    \item Publicités
 			   \pause
			    \item Marketing
			\end{list}
		  \end{alertblock}
		\end{column}
	\end{columns}
      \end{frame}
      \subsection{Le caractère valeur ajoutée des services}
      \begin{frame}{Pourquoi valeur ajoutée ? \hfill\insertframenumber/\inserttotalframenumber\hspace{0.3em}\null}
	  
	  \pause
	   \begin{list}{}{}
	    
	    \begin{exampleblock}{} \hspace{0.05cm} \item  $\clubsuit$ \centering Sauver de l'argent \end{exampleblock}
	                               
	    \pause
	    
	    \begin{exampleblock}{}\hspace{0.05cm } \item  $\clubsuit$ \centering Sauver du temps \end{exampleblock}
	     \pause
	    
	    \begin{exampleblock}{}\hspace{0.05cm } \item  $\clubsuit$ \centering Même sauver des \textbf{VIES} \end{exampleblock}
	   \end{list}
      \end{frame}
      \subsubsection{Quelques cas pratiques}
	\begin{frame}{Cas de la publicité ciblée ... \hfill\insertframenumber/\inserttotalframenumber\hspace{0.3em}\null}
	  
	 \begin{columns}
	      \begin{column}{5cm}
		  \begin{alertblock}{Pour l'utilisateur}
		      \begin{list}{$\clubsuit$}{}
			   \pause
			    \item Service très pertinent
 			   \pause
			    \item Confort accru
% 			    
			\end{list}
		  \end{alertblock}
	      \end{column}
	      \hspace{1cm}
	      \begin{column}{5cm}
		  \begin{alertblock}{Pour la compagnie d'annonces}
		     \begin{list}{$\clubsuit$}{}
 			   \pause 
			    \item Bonne cible
 			   \pause
			    \item Bon lieu
			    \pause
			    \item Bon Moment
% 			    
			\end{list}
		  \end{alertblock}
		\end{column}
	\end{columns}
	\begin{alertblock}{Pour l'opérateur }
		     \begin{list}{$\clubsuit$}{}
 			   \pause
			    \item Augmentation du flux de revenus 
 			   \pause
			    \item Le 1er choix des entreprises pour les publicités
% 			    
			\end{list}
		  \end{alertblock}
      \end{frame}
      
      \begin{frame}{Cas des services d’information à temps réel ... \hfill\insertframenumber/\inserttotalframenumber\hspace{0.3em}\null}
	  
	 \begin{columns}
	      \begin{column}{5cm}
		  \begin{alertblock}{Pour l'utilisateur}
		      \begin{list}{$\clubsuit$}{}
			   \pause
			    \item Service vital
 			   \pause
			    \item Confort accru
% 			    
			\end{list}
		  \end{alertblock}
	      \end{column}
	      \hspace{1cm}
	      \begin{column}{5cm}
		  \begin{alertblock}{Pour l'opérateur }
		     \begin{list}{$\clubsuit$}{}
 			   \pause
			    \item Sauveur ou super héro
 			   \pause
			    \item Augmentation du flux de revenus
% 			    
			\end{list}
		  \end{alertblock}
		\end{column}
	\end{columns}
	
      \end{frame}
      
      \begin{frame}{Cas des services d’alerte ... \hfill\insertframenumber/\inserttotalframenumber\hspace{0.3em}\null}
	  
		  \begin{alertblock}{}
		      \begin{list}{$\clubsuit$}{}
			   \pause
			    \item Prévenir les dangers
 			   \pause
			    \item Alertes qualité service par rapport à leur position
% 			    
			\end{list}
		  \end{alertblock}
	  	  
      \end{frame}
      \section{Quelques inconvénients liés aux services géolocalisés }
      \subsection{Violation de la vie privée}
      \begin{center}
       \begin{frame}{Les services de localisation sont bien  mais ... \hfill\insertframenumber/\inserttotalframenumber\hspace{0.3em}\null}
	  
		  \begin{alertblock}{}
		      \begin{list}{$\clubsuit$}{}
			   \pause
			    \item Accès aux informations personnelles de l'utilisateur sans son autorisation
% 			    
			\end{list}
		  \end{alertblock}
      \end{frame}
      
      \end{center}
       \subsection{Exposition des utilisateurs aux cybercriminels}
      \begin{center}
       \begin{frame}{Les services de localisation sont bien  mais ... \hfill\insertframenumber/\inserttotalframenumber\hspace{0.3em}\null}
	  
		  \begin{alertblock}{}
		      \begin{list}{$\clubsuit$}{}
			   \pause
			    \item Faux guide en détournant le signal du GPS
			    
			    \pause
			    \item Exemple d'application: Domaine militaire 
% 			    
			\end{list}
		  \end{alertblock}
      \end{frame}
      
      \end{center}
      
       \section{Conclusion}
      \begin{center}
       \begin{frame}{En résumé, ... \hfill\insertframenumber/\inserttotalframenumber\hspace{0.3em}\null}
	  
		  \begin{alertblock}{}
		      \begin{list}{$\clubsuit$}{}
			   \pause
			    \item Facteur amélioratif de notre vie quotidienne
			    
			    \pause
			    \item Mais nous expose à des dangers dont il faut être conscient
% 			    
			\end{list}
		  \end{alertblock}
      \end{frame}
      
      \end{center}
      \begin{frame}{Merci pour l'attention! \hfill\insertframenumber/\inserttotalframenumber\hspace{0.3em}\null}
	  
    \begin{figure}[htp]
	      \centering
	      \includegraphics[scale=0.23]{img/questions.jpg}\\
% 	      \caption{Débarquement Allié}
% 	      \label{figure:}
	  \end{figure}
      \end{frame}
      
    \end{center}
\end{document}

