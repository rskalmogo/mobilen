\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{Introduction }{7}{0}{1}
\beamer@subsectionintoc {1}{1}{D\IeC {\'e}finition de la g\IeC {\'e}olocalisation}{8}{0}{1}
\beamer@subsectionintoc {1}{2}{Historique de la g\IeC {\'e}olocalisation}{12}{0}{1}
\beamer@sectionintoc {2}{Diff\IeC {\'e}rentes approches dans la g\IeC {\'e}olocalisation}{25}{0}{2}
\beamer@subsectionintoc {2}{1}{G\IeC {\'e}olocalisation par satellite}{26}{0}{2}
\beamer@subsectionintoc {2}{2}{G\IeC {\'e}olocalisation par t\IeC {\'e}l\IeC {\'e}phone portable}{28}{0}{2}
\beamer@sectionintoc {3}{Services de localisation \IeC {\`a} valeur ajout\IeC {\'e}e}{30}{0}{3}
\beamer@subsectionintoc {3}{1}{D\IeC {\'e}finition}{31}{0}{3}
\beamer@subsectionintoc {3}{2}{Fonctionnement des services g\IeC {\'e}olocalis\IeC {\'e}s}{34}{0}{3}
\beamer@subsectionintoc {3}{3}{Classification des services}{37}{0}{3}
\beamer@subsectionintoc {3}{4}{Quelques exemples d'applications}{42}{0}{3}
\beamer@subsectionintoc {3}{5}{Le caract\IeC {\`e}re valeur ajout\IeC {\'e}e des services}{51}{0}{3}
\beamer@subsubsectionintoc {3}{5}{1}{Quelques cas pratiques}{55}{0}{3}
\beamer@sectionintoc {4}{Quelques inconv\IeC {\'e}nients li\IeC {\'e}s aux services g\IeC {\'e}olocalis\IeC {\'e}s }{71}{0}{4}
\beamer@subsectionintoc {4}{1}{Violation de la vie priv\IeC {\'e}e}{72}{0}{4}
\beamer@subsectionintoc {4}{2}{Exposition des utilisateurs aux cybercriminels}{74}{0}{4}
\beamer@sectionintoc {5}{Conclusion}{77}{0}{5}
